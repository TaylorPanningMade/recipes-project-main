from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

USER_MODEL = settings.AUTH_USER_MODEL


# Create your models here.
class MealPlan(models.Model):
    name = models.CharField(max_length=125)
    author = models.ForeignKey(
        USER_MODEL, related_name="meal_plan", on_delete=models.CASCADE
    )
    date = models.DateTimeField(auto_now_add=False)
    recipe = models.ManyToManyField("recipes.Recipe", related_name="meal_plan")

    def __str__(self):
        return self.name
