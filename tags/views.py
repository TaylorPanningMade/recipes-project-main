from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView

from tags.models import Tag


# Create your views here.
def show_tags(request):
    context = {
        "tags": Tag.objects.all() if Tag else None,
    }
    return render(request, "tags/list.html", context)


class TagDeleteView(DeleteView):
    model = Tag
    template_name = "tags/delete.html"
    success_url = reverse_lazy("tags_list")


class TagUpdateView(UpdateView):
    model = Tag
    template_name = "tags/update.html"
    success_url = reverse_lazy("tags_list")


class TagCreateView(CreateView):
    model = Tag
    template_name = "tags/new.html"
    fields = ["name"]
    success_url = reverse_lazy("tags_list")


class TagDetailView(DetailView):
    model = Tag
    template_name = "tags/detail.html"
    success_url = reverse_lazy("tags_list")


class TagListView(ListView):
    model = Tag
    template_name = "tags/list.html"
    success_url = reverse_lazy("tags_list")

    def get_queryset(self):
        querystring = self.request.GET.get("q")
        if querystring is None:
            querystring = ""
        return Tag.objects.filter(name__icontains=querystring)
